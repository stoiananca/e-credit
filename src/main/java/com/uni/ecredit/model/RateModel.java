package com.uni.ecredit.model;

import lombok.*;

@RequiredArgsConstructor
@Getter
@Setter
public class RateModel {
    private int money;
    private int years;
}
