package com.uni.ecredit.model;

import com.uni.ecredit.util.StringUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id",  "money", "bank", "years", "clientphone", "hash", "previoushash", "timestamp", "nonce"})
@Entity
@Table(name = "request", schema = "ecredit", catalog = "")
public class Request {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String money;

    private String bank;

    private String years;

    private String clientphone;

    private String hash;

    private String previoushash;

    private String timestamp;

    private Integer nonce;

    //Request Constructor.
    public Request(String money, String bank, String years, String clientPhoneNr, String previousHash) {
        this.money = money;
        this.bank = bank;
        this.years = years;
        this.clientphone = clientPhoneNr;
        this.previoushash = previousHash;
        this.timestamp = String.valueOf(new Date().getTime());
        this.nonce = 0;

        this.hash = calculateHash(); //Making sure we do this after we set the other values.
    }

    //Calculate new hash based on blocks contents
    public String calculateHash() {
        return StringUtil.applySha256(
                previoushash +
                        timestamp +
                        money +
                        Integer.toString(nonce)
        );
    }

    //Increases nonce value until hash target is reached.
    public void mineBlock(int difficulty) {
        String target = StringUtil.getDificultyString(difficulty); //Create a string with difficulty * "0"
        while(!hash.substring( 0, difficulty).equals(target)) {
            nonce ++;
            hash = calculateHash();
        }
    }
}
