package com.uni.ecredit.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id", "alias",  "description", "creditnewhome", "interestrate"})
@Entity
@Table(name = "bank", schema = "ecredit", catalog = "")
public class Bank {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String alias;

    private String description;

    private Integer creditnewhome;

    private double interestrate;


}
