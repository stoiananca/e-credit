package com.uni.ecredit.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id", "advance", "credit", "currency", "homevalue", "hometype", "creditduration", "rates"
        , "interestratetype", "interestrate", "codebitors"})
@Entity
@Table(name = "creditnewhome", schema = "ecredit", catalog = "")
public class CreditNewHome {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String advance;

    private String credit;

    private String currency;

    private String homevalue;

    private String hometype;

    private String creditduration;

    private String rates;

    private String interestratetype;

    private String interestrate;

    private String codebitors;

}
