package com.uni.ecredit.model;

import com.uni.ecredit.util.StringUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id",  "money", "bank", "years", "clientphone", "status"})
@Entity
@Table(name = "pendingreq", schema = "ecredit", catalog = "")
public class PendingRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String money;

    private String bank;

    private String years;

    private String clientphone;

    private Integer status;

}
