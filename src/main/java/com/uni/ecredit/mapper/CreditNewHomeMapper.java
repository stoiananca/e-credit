package com.uni.ecredit.mapper;

import com.uni.ecredit.dto.CreditNewHomeDto;
import com.uni.ecredit.model.CreditNewHome;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CreditNewHomeMapper {

    public CreditNewHomeDto mapEntityToDto(CreditNewHome creditNewHome) {
        CreditNewHomeDto creditNewHomeDto = new CreditNewHomeDto();

        if (null == creditNewHome) return null;

        creditNewHomeDto.setId(creditNewHome.getId());
        creditNewHomeDto.setAdvance(creditNewHome.getAdvance());
        creditNewHomeDto.setCredit(creditNewHome.getCredit());
        creditNewHomeDto.setCurrency(creditNewHome.getCurrency());
        creditNewHomeDto.setHomevalue(creditNewHome.getHomevalue());
        creditNewHomeDto.setHometype(creditNewHome.getHometype());
        creditNewHomeDto.setCreditduration(creditNewHome.getCreditduration());
        creditNewHomeDto.setRates(creditNewHome.getRates());
        creditNewHomeDto.setInterestratetype(creditNewHome.getInterestratetype());
        creditNewHomeDto.setInterestrate(creditNewHome.getInterestrate());
        creditNewHomeDto.setCodebitors(creditNewHome.getCodebitors());
        return creditNewHomeDto;
    }

    public List<CreditNewHomeDto> mapEntityToDto(List<CreditNewHome> creditNewHomeList) {
        if (null == creditNewHomeList) return null;
        return creditNewHomeList.stream()
                .map(this::mapEntityToDto)
                .collect(Collectors.toCollection(() -> new ArrayList<>(0)));
    }
}
