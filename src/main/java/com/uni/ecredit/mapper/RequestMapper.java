package com.uni.ecredit.mapper;

import com.uni.ecredit.dto.RequestDto;
import com.uni.ecredit.dto.UserDto;
import com.uni.ecredit.model.Request;
import com.uni.ecredit.model.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RequestMapper {

    public RequestDto mapEntityToDto(Request request) {
        RequestDto requestDto = new RequestDto();

        if (null == request) return null;

        requestDto.setId(request.getId());
        requestDto.setMoney(request.getMoney());
        requestDto.setBank(request.getBank());
        requestDto.setYears(request.getYears());
        requestDto.setClientphone(request.getClientphone());
        requestDto.setHash(request.getHash());
        requestDto.setPrevioushash(request.getPrevioushash());
        requestDto.setTimestamp(request.getTimestamp());
        requestDto.setNonce(request.getNonce());
        return requestDto;
    }

    public List<RequestDto> mapEntityToDto(List<Request> requestList) {
        if (null == requestList) return null;
        return requestList.stream()
                .map(this::mapEntityToDto)
                .collect(Collectors.toCollection(() -> new ArrayList<>(0)));
    }
}
