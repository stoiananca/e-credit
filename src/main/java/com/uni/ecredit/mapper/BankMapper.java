package com.uni.ecredit.mapper;

import com.uni.ecredit.dto.BankDto;
import com.uni.ecredit.dto.UserDto;
import com.uni.ecredit.model.Bank;
import com.uni.ecredit.model.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class BankMapper {

    public BankDto mapEntityToDto(Bank bank) {
        BankDto bankDto = new BankDto();

        if (null == bank) return null;

        bankDto.setId(bank.getId());
        bankDto.setAlias(bank.getAlias());
        bankDto.setDescription(bank.getDescription());
        bankDto.setCreditnewhome(bank.getCreditnewhome());
        bankDto.setInterestrate(bank.getInterestrate());
        return bankDto;
    }

    public List<BankDto> mapEntityToDto(List<Bank> bankList) {
        if (null == bankList) return null;
        return bankList.stream()
                .map(this::mapEntityToDto)
                .collect(Collectors.toCollection(() -> new ArrayList<>(0)));
    }
}
