package com.uni.ecredit.mapper;

import com.uni.ecredit.dto.PendingRequestDto;
import com.uni.ecredit.dto.RequestDto;
import com.uni.ecredit.model.PendingRequest;
import com.uni.ecredit.model.Request;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PendingRequestMapper {

    public PendingRequestDto mapEntityToDto(PendingRequest request) {
        PendingRequestDto requestDto = new PendingRequestDto();

        if (null == request) return null;

        requestDto.setId(request.getId());
        requestDto.setMoney(request.getMoney());
        requestDto.setBank(request.getBank());
        requestDto.setYears(request.getYears());
        requestDto.setClientphone(request.getClientphone());
        requestDto.setStatus(request.getStatus());
        return requestDto;
    }

    public List<PendingRequestDto> mapEntityToDto(List<PendingRequest> requestList) {
        if (null == requestList) return null;
        return requestList.stream()
                .map(this::mapEntityToDto)
                .collect(Collectors.toCollection(() -> new ArrayList<>(0)));
    }
}
