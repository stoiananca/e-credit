package com.uni.ecredit.mapper;

import com.uni.ecredit.dto.UserDto;
import com.uni.ecredit.model.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapper {

    public UserDto mapEntityToDto(User user) {
        UserDto userDto = new UserDto();

        if (null == user) return null;

        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setPhone(user.getPhone());
        userDto.setEmail(user.getEmail());
        userDto.setRequest(user.getRequest());
        userDto.setPassword(user.getPassword());
        userDto.setUsername(user.getUsername());
        return userDto;
    }

    public User mapDtotOEntity(UserDto userDto) {
        User user = new User();

        if (null == userDto) return null;

        user.setName(userDto.getName());
        user.setPhone(userDto.getPhone());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setUsername(userDto.getUsername());
        return user;
    }

    public List<UserDto> mapEntityToDto(List<User> userList) {
        if (null == userList) return null;
        return userList.stream()
                .map(this::mapEntityToDto)
                .collect(Collectors.toCollection(() -> new ArrayList<>(0)));
    }
}
