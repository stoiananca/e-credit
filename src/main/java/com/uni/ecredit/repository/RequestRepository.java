package com.uni.ecredit.repository;

import com.uni.ecredit.model.Request;
import com.uni.ecredit.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RequestRepository extends JpaRepository<Request, Integer> {

    @Query(value = "SELECT ecredit.Request.hash FROM ecredit.Request ORDER BY id DESC LIMIT 1", nativeQuery = true)
    String getPreviousHash();
}
