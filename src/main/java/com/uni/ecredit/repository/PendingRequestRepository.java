package com.uni.ecredit.repository;

import com.uni.ecredit.model.PendingRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PendingRequestRepository extends JpaRepository<PendingRequest, Integer> {
}

