package com.uni.ecredit.repository;

import com.uni.ecredit.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
}
