package com.uni.ecredit.repository;

import com.uni.ecredit.model.CreditNewHome;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CreditNewHomeRepository extends JpaRepository<CreditNewHome, Integer> {
}
