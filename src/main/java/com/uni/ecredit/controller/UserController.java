package com.uni.ecredit.controller;

import com.uni.ecredit.dto.UserDto;
import com.uni.ecredit.mapper.UserMapper;
import com.uni.ecredit.model.User;
import com.uni.ecredit.service.LoginService;
import com.uni.ecredit.service.UserService;
import com.uni.ecredit.util.AES;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;

    @GetMapping("/getUserList")
    public List<UserDto> getListUser() {
        List<User> userEntityList = userService.getAllUsers();
        return userMapper.mapEntityToDto(userEntityList);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getUserInfo")
    public User getUserInfo() {
        for (User userInList : userService.getAllUsers()) {
            if(userInList.getUsername().equals(LoginService.currentUser))
                return userInList;
        }
        return null;
    }

    @GetMapping("/saveUsers")
    public void saveUsers() {
        userService.saveUsers();
    }

    @GetMapping("/deleteUsers")
    public void deleteUsers() {
        userService.deleteUsers();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/isAdministrator")
    public boolean isAdministrator() {
        return userService.isAdministrator();
    }
}
