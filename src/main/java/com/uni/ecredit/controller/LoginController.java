package com.uni.ecredit.controller;

import com.uni.ecredit.dto.UserDto;
import com.uni.ecredit.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class LoginController {

    private final LoginService loginService;

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/login")
    public boolean login(@RequestBody UserDto user) {
        return loginService.login(user);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/register")
    public boolean register(@RequestBody UserDto user) {
        return loginService.register(user);
    }

}
