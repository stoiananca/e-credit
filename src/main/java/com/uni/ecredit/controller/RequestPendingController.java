package com.uni.ecredit.controller;

import com.uni.ecredit.dto.BankDto;
import com.uni.ecredit.dto.PendingRequestDto;
import com.uni.ecredit.mapper.BankMapper;
import com.uni.ecredit.mapper.PendingRequestMapper;
import com.uni.ecredit.model.Bank;
import com.uni.ecredit.model.PendingRequest;
import com.uni.ecredit.model.RateModel;
import com.uni.ecredit.model.Request;
import com.uni.ecredit.service.BankService;
import com.uni.ecredit.service.PendingRequestService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class RequestPendingController {

    private final PendingRequestService pendingRequestService;
    private final PendingRequestMapper pendingRequestMapper;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getPendingRequests")
    public List<PendingRequestDto> getPendingRequests() {
        List<PendingRequest> pendingRequestList = pendingRequestService.getAllPendingRequests();
        return pendingRequestMapper.mapEntityToDto(pendingRequestList);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/insertPendingRequest")
    public void insertRequest(@RequestBody PendingRequest request) {
        pendingRequestService.insertRequest(request);
    }

    @GetMapping("/deletePendingRequests")
    public void deleteBanks() {
        pendingRequestService.deletePendingRequests();
    }

}
