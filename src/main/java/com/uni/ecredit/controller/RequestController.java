package com.uni.ecredit.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.uni.ecredit.dto.RequestDto;
import com.uni.ecredit.mapper.RequestMapper;
import com.uni.ecredit.model.Request;
import com.uni.ecredit.service.RequestService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class RequestController {

    private final RequestService requestService;
    private final RequestMapper requestMapper;

    @GetMapping("/getVotes")
    public List<RequestDto> getVotes() {
        List<Request> requestEntityList = requestService.getAllRequests();
        return requestMapper.mapEntityToDto(requestEntityList);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/insertRequest")
    public void insertRequest(@RequestBody String no) {
        requestService.insertRequest(no);
    }

    @GetMapping("/checkValidity")
    public Boolean checkValidity() {
        return requestService.isChainValid();
    }

    @GetMapping("/deleteRequests")
    public void deleteRequests() {
        requestService.deleteRequests();
    }

}
