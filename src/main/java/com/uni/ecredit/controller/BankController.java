package com.uni.ecredit.controller;

import com.uni.ecredit.dto.BankDto;
import com.uni.ecredit.dto.UserDto;
import com.uni.ecredit.mapper.BankMapper;
import com.uni.ecredit.mapper.UserMapper;
import com.uni.ecredit.model.Bank;
import com.uni.ecredit.model.RateModel;
import com.uni.ecredit.model.User;
import com.uni.ecredit.service.BankService;
import com.uni.ecredit.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class BankController {

    private final BankService bankService;
    private final BankMapper bankMapper;

    @GetMapping("/getBankList")
    public List<BankDto> getListUser() {
        List<Bank> bankEntityList = bankService.getAllBanks();
        return bankMapper.mapEntityToDto(bankEntityList);
    }

    @GetMapping("/saveBanks")
    public void saveBanks() {
        bankService.saveBanks();
    }

    @GetMapping("/deleteBanks")
    public void deleteBanks() {
        bankService.deleteBanks();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/calculate")
    public List<String> calculate(@RequestBody RateModel rateModel) {
        return bankService.calculate(rateModel);
    }

}
