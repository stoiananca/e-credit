package com.uni.ecredit.controller;

import com.uni.ecredit.dto.BankDto;
import com.uni.ecredit.dto.CreditNewHomeDto;
import com.uni.ecredit.mapper.BankMapper;
import com.uni.ecredit.mapper.CreditNewHomeMapper;
import com.uni.ecredit.model.Bank;
import com.uni.ecredit.model.CreditNewHome;
import com.uni.ecredit.service.BankService;
import com.uni.ecredit.service.CreditNewHomeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CreditNewHomeController {

    private final CreditNewHomeService creditNewHomeService;
    private final CreditNewHomeMapper creditNewHomeMapper;

    @GetMapping("/getCreditNewHomeList")
    public List<CreditNewHomeDto> getListUser() {
        List<CreditNewHome> creditNewHomeEntityList = creditNewHomeService.getAllCreditNewHomes();
        return creditNewHomeMapper.mapEntityToDto(creditNewHomeEntityList);
    }

    @GetMapping("/saveCreditNewHomes")
    public void saveCreditNewHomes() {
        creditNewHomeService.saveCreditNewHomes();
    }

    @GetMapping("/deleteCreditNewHomes")
    public void deleteCreditNewHomes() {
        creditNewHomeService.deleteCreditNewHomes();
    }
}
