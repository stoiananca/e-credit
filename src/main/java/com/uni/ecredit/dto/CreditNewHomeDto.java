package com.uni.ecredit.dto;

import lombok.Data;

@Data
public class CreditNewHomeDto {
    private Integer id;
    private String advance;
    private String credit;
    private String currency;
    private String homevalue;
    private String hometype;
    private String creditduration;
    private String rates;
    private String interestratetype;
    private String interestrate;
    private String codebitors;
}
