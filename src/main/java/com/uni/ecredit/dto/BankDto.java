package com.uni.ecredit.dto;

import lombok.Data;

@Data
public class BankDto {
    private Integer id;
    private String alias;
    private String description;
    private Integer creditnewhome;
    private double interestrate;
}
