package com.uni.ecredit.dto;

import lombok.Data;

@Data
public class RequestDto {
    private Integer id;
    private String money;
    private String bank;
    private String years;
    private String clientphone;
    private String hash;
    private String previoushash;
    private String timestamp;
    private Integer nonce;
}
