package com.uni.ecredit.dto;

import lombok.Data;

@Data
public class UserDto {
    private Integer id;
    private String name;
    private String phone;
    private String email;
    private Integer request;
    private String password;
    private String username;
}
