package com.uni.ecredit.dto;

import lombok.Data;

@Data
public class PendingRequestDto {
    private Integer id;
    private String money;
    private String bank;
    private String years;
    private String clientphone;
    private Integer status;
}
