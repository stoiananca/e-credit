package com.uni.ecredit.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uni.ecredit.model.PendingRequest;
import com.uni.ecredit.model.Request;
import com.uni.ecredit.model.User;
import com.uni.ecredit.repository.RequestRepository;
import com.uni.ecredit.repository.UserRepository;
import com.uni.ecredit.util.AES;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RequestService {
    private final String secretKey = "secretKey";

    public static int difficulty = 5;

    private final RequestRepository requestRepository;
    private final UserService userService;
    private final PendingRequestService pendingRequestService;

    public List<Request> getAllRequests() {
        return requestRepository.findAll();
    }

    public void insertRequest(String no) {
        PendingRequest pendingRequest = pendingRequestService.getAllPendingRequests().get(Integer.parseInt(no)-1);
        Request request = new Request();
        request.setBank(pendingRequest.getBank());
        request.setClientphone(pendingRequest.getClientphone());
        request.setMoney(pendingRequest.getMoney());
        request.setYears(pendingRequest.getYears());

        final boolean[] hasRequested = {false};
        userService.getAllUsers().forEach(user -> {
            if (user.getEmail().equals(LoginService.currentUser)){
                hasRequested[0] = user.getRequest() == 1;
            }
        });
        Request newRequest = null;

        if (null == getAllRequests() || getAllRequests().isEmpty()) {
            newRequest = new Request(AES.encrypt(request.getMoney(), secretKey), request.getBank(), request.getYears(), request.getClientphone(), String.valueOf(0));
        } else {
            if(!hasRequested[0]) {
                newRequest = new Request(AES.encrypt(request.getMoney(), secretKey), request.getBank(), request.getYears(), request.getClientphone(), requestRepository.getPreviousHash());
            }
        }

        pendingRequestService.updatePendingRequest(LoginService.currentUser);

        if(null != newRequest) {
            newRequest.mineBlock(difficulty);
            requestRepository.save(newRequest);
        }
    }

    public Boolean isChainValid() {
        List<Request> requestList = requestRepository.findAll();
        String hashTarget = new String(new char[difficulty]).replace('\0', '0');

        //loop through blockchain to check hashes:
        for (int i = 0; i < requestList.size() - 1; i++) {
            Request currentBlock = requestList.get(i + 1);
            Request previousBlock = requestList.get(i);

            //compare registered hash and calculated hash:
            if (!currentBlock.getHash().equals(currentBlock.calculateHash())) {
                System.out.println("Current Hashes not equal");
                return false;
            }
            //compare previous hash and registered previous hash
            if (!previousBlock.getHash().equals(currentBlock.getPrevioushash())) {
                System.out.println("Previous Hashes not equal");
                return false;
            }
            //check if hash is solved
            if (!currentBlock.getHash().substring(0, difficulty).equals(hashTarget)) {
                System.out.println("This block hasn't been mined");
                return false;
            }
        }
        return true;
    }
    public void deleteRequests() {
        requestRepository.deleteAll();
    }

}
