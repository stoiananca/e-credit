package com.uni.ecredit.service;

import com.uni.ecredit.model.Bank;
import com.uni.ecredit.model.RateModel;
import com.uni.ecredit.repository.BankRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class BankService {

    private final BankRepository bankRepository;

    public List<Bank> getAllBanks() {
        return bankRepository.findAll();
    }

    public void saveBanks() {
        List<Bank> bankList = new ArrayList<>();
        bankList.add(new Bank(1, "ING", "Descriere ING", 1, 3.45));
        bankList.add(new Bank(2, "BRD", "Descriere BRD", 1, 4.22));
        bankList.add(new Bank(3, "BCR", "Descriere BCR", 1, 2.90));

        bankRepository.saveAll(bankList);
    }

    public void deleteBanks() {
        bankRepository.deleteAll();
    }

    public List<String> calculate( RateModel rateModel) {
        List<Double> list = new ArrayList<>();
        List<String> finalList = new ArrayList<>();

        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.DOWN);

        getAllBanks().forEach(bank -> list.add((rateModel.getMoney() * bank.getInterestrate() / 1200) / (1 - Math.pow((1 + bank.getInterestrate() / 1200), (-rateModel.getYears() * 12)))));
        list.forEach(aDouble -> finalList.add(df.format(aDouble)));

        return finalList;
    }

}
