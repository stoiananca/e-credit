package com.uni.ecredit.service;

import com.uni.ecredit.model.CreditNewHome;
import com.uni.ecredit.repository.CreditNewHomeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CreditNewHomeService {

    private final CreditNewHomeRepository creditNewHomeRepository;

    public List<CreditNewHome> getAllCreditNewHomes() {
        return creditNewHomeRepository.findAll();
    }

    public void saveCreditNewHomes() {
        List<CreditNewHome> creditNewHomeList = new ArrayList<>();
        creditNewHomeList.add(new CreditNewHome(1, "Minim 5%", "1.000€ - 66.500€", "Lei", "Pana la 70.000€", "Noua\n" +
                "Veche\n" +
                "Consolidata", "Maxim 30 de ani", "Principal si dobanda", "Variabila", "Rata Fixa 2% + IRCC",
                "Pana la 3 codebitori"));
        creditNewHomeList.add(new CreditNewHome(2, "Minim 15%", "59.501€ - 119.000€", "Lei", "Pana la 140.000€", "Noua\n" +
                "In curs de construire", "Maxim 30 de ani", "Principal si dobanda", "Variabila", "Rata Fixa 2% + IRCC",
                "Pana la 3 codebitori"));

        creditNewHomeRepository.saveAll(creditNewHomeList);
    }

    public void deleteCreditNewHomes() {
        creditNewHomeRepository.deleteAll();
    }

}
