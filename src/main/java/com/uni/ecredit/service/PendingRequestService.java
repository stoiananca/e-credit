package com.uni.ecredit.service;

import com.uni.ecredit.model.Bank;
import com.uni.ecredit.model.PendingRequest;
import com.uni.ecredit.model.RateModel;
import com.uni.ecredit.model.Request;
import com.uni.ecredit.repository.BankRepository;
import com.uni.ecredit.repository.PendingRequestRepository;
import com.uni.ecredit.repository.RequestRepository;
import com.uni.ecredit.util.AES;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PendingRequestService {
    private final PendingRequestRepository pendingRequestRepository;
    private final UserService userService;

    public List<PendingRequest> getAllPendingRequests() {
        return pendingRequestRepository.findAll();
    }

    public void insertRequest(PendingRequest pendingRequest) {
        userService.updateUser(LoginService.currentUser);
        pendingRequest.setStatus(0);
        pendingRequestRepository.save(pendingRequest);
    }

    public void updatePendingRequest(String username){
        final String[] clientPhoneNr = new String[1];
        userService.getAllUsers().forEach(user -> {
            if(user.getUsername().equals(username)){
                clientPhoneNr[0] = user.getPhone();
            }
        });
        getAllPendingRequests().forEach(request -> {
            if(request.getClientphone().equals( clientPhoneNr[0])){
                request.setStatus(1);
                pendingRequestRepository.save(request);
            }
        });
    }

    public void deletePendingRequests() {
        pendingRequestRepository.deleteAll();
    }

}
