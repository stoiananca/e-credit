package com.uni.ecredit.service;

import com.uni.ecredit.model.User;
import com.uni.ecredit.repository.UserRepository;
import com.uni.ecredit.util.AES;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {
    private final String secretKey = "secretKey";

    private final UserRepository userRepository;

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public void saveUsers() {
        List<User> userList = new ArrayList<>();
        userList.add(new User(1, "Stoian Anca", "stoian.anca@yahoo.com", "0731343049", 0, AES.encrypt("pass1", secretKey), "stoiananca"));
        userList.add(new User(2, "Popescu Mihai", "popescu.mihai@yahoo.com", "0723456789", 0, AES.encrypt("pass2", secretKey), "popescumihai"));
        userList.add(new User(3, "Ionescu Andrada", "ionescu.andrada@yahoo.com", "0798765432", 0, AES.encrypt("pass3", secretKey), "ionescuandrada"));

        userRepository.saveAll(userList);
    }

    public void deleteUsers() {
        userRepository.deleteAll();
    }

    public void updateUser(String email) {
        getAllUsers().forEach(user -> {
            if (user.getEmail().equals(email)) {
                user.setRequest(1);
                userRepository.save(user);
            }
        });
    }

    public boolean isAdministrator() {
        return LoginService.currentUser.equals("popescumihai");
    }
}
