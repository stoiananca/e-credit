package com.uni.ecredit.service;

import com.uni.ecredit.mapper.UserMapper;
import com.uni.ecredit.repository.UserRepository;
import com.uni.ecredit.util.AES;
import com.uni.ecredit.dto.UserDto;
import com.uni.ecredit.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final String secretKey = "secretKey";

    public static String currentUser;

    private final UserService userService;

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserRepository userRepository;

    public boolean login(UserDto userDto) {
        for (User userInList : userService.getAllUsers()) {
            if (userInList.getUsername().equals(userDto.getUsername()) && userDto.getPassword().equals(AES.decrypt(userInList.getPassword(), secretKey))) {
                currentUser=userDto.getUsername();
                return true;
            }
        }
        return false;
    }

    public boolean register(UserDto userDto) {
       User user = userMapper.mapDtotOEntity(userDto);

       user.setPassword(AES.encrypt(user.getPassword(), secretKey));
       user.setRequest(0);

       userRepository.save(user);
       return true;
    }
}
